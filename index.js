const pup = require('puppeteer')
const opts = require('./creds')

const iUsername = 'input[name=username]'
const iPassword = 'input[name=password]'
const bLogin =
  '#react-root > section > main > div > article > div > div:nth-child(1) > div > form > span > button'
const bLike =
  '#react-root > section > main > div > div > article > div.eo2As > section.ltpMr.Slqrh > a.fr66n.tiVCN > span'

async function run() {
  const browser = await pup.launch({ headless: true })

  try {
    const tab = await browser.newPage()

    tab.on('console', msg => {
      for (let i = 0; i < msg.args.length; ++i)
        console.log(`${i}: ${msg.args[i]}`)
    })

    await tab.goto('https://www.instagram.com/accounts/login/')

    await tab.waitForSelector(iUsername)
    await tab.focus(iUsername)
    await tab.keyboard.type(opts.username)

    await tab.focus(iPassword)
    await tab.keyboard.type(opts.password)

    await tab.click(bLogin)

    await tab.waitForSelector('span.coreSpriteSearchIcon', {
      waitUntil: 'networkidle2',
    })

    for (let USERNAME of ['balinux']) {
      console.log(`visiting https://www.instagram.com/${USERNAME}`)
      await tab.goto(`https://www.instagram.com/${USERNAME}`)

      const imageLinks = await tab.evaluate(() => {
        const elements = Array.from(document.querySelectorAll('a[href]'))
        return elements
          .map(anchor => anchor.getAttribute('href'))
          .filter(link => link.startsWith('/p/'))
          .map(link => {
            return `https://www.instagram.com${link}`
          })
      })

      for (const image of imageLinks) {
        await tab.goto(image, { waitUntil: 'networkidle2' })

        if ((await tab.$('span.coreSpriteHeartOpen')) !== null) {
          await tab.click(bLike)
          console.log('liked')
        } else {
          console.log('already liked')
        }
      }
    }

    console.log('done')
  } catch (e) {
    console.log(e)
  }

  browser.close()
}

run()
